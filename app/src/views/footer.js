export default <footer className='pv4 ph3 ph5-m ph6-l mid-gray right-0 bottom-0 left-0'>
  <small className='f6 db tc'>© 2016 <b className='ttu'>2fa.codes</b></small>
  <div className='tc mt3'>
    <a href='/about.html' className='f6 dib ph2 link mid-gray dim'>About</a>
  </div>
</footer>
