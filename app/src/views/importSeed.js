// export default <div>
//   <form autoComplete='off' id='import-form'>
//     <div className='pv1 pv2-ns'>
//       <label htmlFor='name'>
//         <code className='f6 db'>Account name</code>
//       </label>
//       <input
//         id='name'
//         type='text'
//         className='input-reset w-100 br2 ba'
//         required
//         placeholder='Account name'
//         autoComplete='off' />
//     </div>
//     <div className='pv1 pv2-ns'>
//       <label htmlFor='seed'>
//         <code className='f6 db'>Key</code>
//       </label>
//       <input
//         id='seed'
//         type='text'
//         className='input-reset w-100 br2 ba'
//         required
//         placeholder='Key'
//         autoComplete='off' />
//     </div>
//     <button className='link ba bg-white w-100' type='submit'>
//       Add
//     </button>
//   </form>
//   <hr />
//   <a href='/scan.html'><button className='link ba bg-white w-100 black'>
//     Scan QR Code
//   </button>
//   </a>
// </div>

export default <div className="pa4 black-80 ba b--transparent ph0 mh0">
  <form autoComplete='off' id='import-form' accept-charset="utf-8">
    <div className='pv1 pv2-ns'>
      <label htmlFor='name' className="db fw4 w-100 lh-copy f6">Account name</label>
      <input
        id='name'
        type='text'
        className='b pa2 input-reset ba w-100 bg-transparent'
        required
        placeholder='Account name'
        autoComplete='off' />
    </div>
    <div className='pv1 pv2-ns'>
      <label htmlFor='seed' className="db w-100 fw4 lh-copy f6">Key</label>
      <input
        id='seed'
        type='text'
        className='b pa2 input-reset ba w-100 bg-transparent'
        required
        placeholder='Key'
        autoComplete='off' />
    </div>
    <button className='b ph3 pv2 input-reset w-100 ba b--black bg-transparent pointer f6' type='submit'>
      Add
    </button>
  </form>
  <hr />
  <a href='/scan.html'><button className='link b ph3 pv2 input-reset w-100 ba b--black bg-transparent pointer f6 black'>
    Scan QR Code
  </button>
  </a>
</div>