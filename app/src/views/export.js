import {generateTotpUri} from '../authenticator'
export default ({name, seed}) => <div>
  <img data-src={generateTotpUri(seed, name)} id='export-img' className='w-100 h-100' />
  <span>{seed}</span>
</div>
