function weakPasswordText (passwordTips) {
	if (passwordTips.length === 0) {
		return `Password too simple`
	} else if (passwordTips.endsWith('.')) {
		return `Password too simple: ${passwordTips}`
	} else {
		return `Password too simple: ${passwordTips}.`
	}
}

export default function (wrongPassword, weakPassword, passwordTips) {
	return <form className="pa4 black-80" id='login'>
		<div className="measure-narrow">
			<label htmlFor="password" className="f6 b db mb2">Password</label>
			<input className="input-reset ba b--black-20 pa2 mb2 db w-100" type="password" id='password' required autoComplete='off' />
			{ !wrongPassword ? undefined : <span className="dark-red" >Incorrect password</span> }
			{ !weakPassword ? undefined : <span className="dark-red" >{weakPasswordText(passwordTips)}</span> }
			<button className='link b ph3 pv2 input-reset w-100 ba b--black bg-transparent pointer f6 black' type='submit'>
				Decrypt
			</button>
			<hr />
			<button className='link b ph3 pv2 input-reset w-100 ba b--black bg-transparent pointer f6 black' data-click={{type: 'deleteDatabase'}}>
				Delete database
			</button>
		</div>
	</form>
}