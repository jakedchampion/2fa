export default <div>
  <p>2FA is an application to store your two-factor authentication keys and generate tokens to verify yourself.</p>
  <p>None of your data is sent to any server.All information is stored locally on your device.</p>
</div>
