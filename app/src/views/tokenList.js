'use strict';

const renderListItem = (name, token, seed) => <li className="dt w-100 bb b--black-05 pb2 mt2" href="#0">
      <div className="dtc w2 w3-ns v-mid">
        <img src="https://placekitten.com/g/66/66" className="ba b--black-10 db br2 w2 w3-ns h2 h3-ns"/>
      </div>
      <div className="dtc v-mid pl3">
        <h1 className="f6 f5-ns fw6 lh-title black mv0">{name} </h1>
        <h2 className="f6 fw4 mt0 mb0 black-60">{token}</h2>
      </div>
      <div className="dtc v-mid">
        <form className="w-100 tr">
        <button className='f6 button-reset bg-white ba b--black-10 dim pointer pv1 black-60 mr3' data-click={{type: 'export', payload: {seed, name}}}>export</button>
        <button className='f6 button-reset bg-white ba b--black-10 dim pointer pv1 black-60 mr3' data-click={{type: 'delete', payload: {seed, name}}} >delete</button>
        </form>
      </div>
    </li>

export default (nameSeedPairs) => {
  if (nameSeedPairs.length === 0) {
    return [<div className='pb1' >You have no items.</div>, <div> Add an item using <a className='link dim gray dib' href='/import.html' title='Add new item'>text</a> or <a className='link dim gray dib' href='/scan.html'>scan a QR code</a></div>]
  } else {
    return <ul className='list pl0 ma0' id='list'>
      {nameSeedPairs.map(({name, seed, token}) => renderListItem(name, token, seed))}
    </ul>
  }
}
