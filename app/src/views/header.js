const svg = require('virtual-dom/virtual-hyperscript/svg');

const timer = svg('svg', {
    width: '45px',
    height: '45px',
    viewBox: '0 0 80 80'
  }, [
    svg('path.timer', {
      'd': 'M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z'
    })
  ])

export default (url, showSpinner) => <header>
  <nav className='pa3 pa4-ns '>
    <ul className="list pa0" >
      <li className='dib' >
        <a className={`${(url === '/') ? 'corner-border-active' : ''} corner-border link f6 f5-ns dib pa3 relative gray`} href='/' title='Home'>Home</a>
      </li>
      <li className='dib'>
        <a className={`${(url === '/import.html') ? 'corner-border-active' : ''} corner-border link f6 f5-ns dib pa3 relative gray`} href='/import.html' title='Add new item'>Add new item</a>
      </li>
      <li className='dib'>
        <a className={`${(url === '/scan.html') ? 'corner-border-active' : ''} corner-border link f6 f5-ns dib pa3 relative gray`} href='/scan.html'>Scan QR Code</a>
      </li>
      <li className='dib  corner-border link f6 f5-ns dib v-mid'>
        {(url === '/' && showSpinner) ? timer : undefined}
      </li>
    </ul>
  </nav>
</header>
