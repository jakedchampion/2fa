import tokenList from './tokenList'
import about from './about'
import header from './header'
import footer from './footer'
import importSeed from './importSeed'
import scan from './scan'
import exportPage from './export'
import login from './login'

export default ({ url, nameSeedPairs, export: exportItem, hasKey, wrongPassword, weakPassword, passwordTips}) => {
  let page
  if (!hasKey) {
    page = login(wrongPassword, weakPassword, passwordTips)
  } else {
    switch (url) {
      case '/': {
        page = tokenList(nameSeedPairs)
        break
      }
      case '/about.html': {
        page = about
        break
      }
      case '/import.html': {
        page = importSeed
        break
      }
      case '/scan.html': {
        page = scan
        break
      }
      case '/export': {
        page = exportPage(exportItem)
        break
      }
    }
  }

  return <div id='app' className='mw6' style='display: flex; flex-direction: column; height: 100vh;'>
    {header(url, !!hasKey)}
    <main className='ph3 ph4-ns' style='flex:1;'>
      {page}
    </main>
    {footer}
  </div>
}
