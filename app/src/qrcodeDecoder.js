var Promise = require('bluebird')

const qrcode = require('zxing')

const decoder = Promise.promisify(qrcode.decode.bind(qrcode))

let stop = false

const _prepareCanvas = function (videoElem) {
  const canvas = document.createElement('canvas')
  canvas.style.width = videoElem.videoWidth + 'px'
  canvas.style.height = videoElem.videoHeight + 'px'
  canvas.width = videoElem.videoWidth
  canvas.height = videoElem.videoHeight
  return canvas
}

function loop (videoElem, gCtx, canvas) {
  return Promise.try(function () {
    if (stop) {
      return Promise.resolve()
    }
    gCtx.clearRect(0, 0, videoElem.videoWidth, videoElem.videoHeight)
    gCtx.drawImage(videoElem, 0, 0, videoElem.videoWidth, videoElem.videoHeight)

    return decoder(canvas.toDataURL())
  }).catch(() => Promise.delay(250).then(() => loop(videoElem, gCtx, canvas)))
}

module.exports = {
  decode (videoElem) {
    stop = false
    const canvas = _prepareCanvas(videoElem)
    const gCtx = canvas.getContext('2d')
    return Promise.try(function () {
      return loop(videoElem, gCtx, canvas)
    })
  },
  stop () {
    stop = true
  }
}
