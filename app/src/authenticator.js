/* eslint-env browser */
const convert = require('./base32')
const totp = require('notp').totp

// Binary-decode the key from base32 (Google Authenticator, FB, M$, etc)
function decodeGoogleAuthKey (key) {
  // decode base32 google auth key to binary
  const unformatted = key.replace(/\W+/g, '').toUpperCase()
  const bin = convert.base32ToBuffer(unformatted)

  return bin
}

module.exports = {
  generateToken (key) {
    const bin = decodeGoogleAuthKey(key)
    return totp.gen(bin)
  },
  generateTotpUri: function (secret, accountName, issuer, algo, digits, period) {
    // Full OTPAUTH URI spec as explained at
    // https://github.com/google/google-authenticator/wiki/Key-Uri-Format
    return 'otpauth://totp/' +encodeURI(accountName || '') +
    '?secret=' + secret.replace(/[\s\.\_\-]+/g, '').toUpperCase() +
    '&issuer=' + encodeURIComponent(issuer || '') +
    '&algorithm=' + (algo || 'SHA1') +
    '&digits=' + (digits || 6) +
    '&period=' + (period || 30)
  }
}
