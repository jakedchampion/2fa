/* global URLSearchParams */
/* eslint-env browser */

import 'tachyons/css/tachyons.min.css'
import './styles.css'
import WorkerThread from './worker.thread'
import virtualize from 'vdom-virtualize'
import toJson from 'vdom-as-json/toJson'
import applyPatch from 'vdom-serialized-patch/patch'
import { getLocalPathname } from 'local-links'
import qrDecoder from './qrcodeDecoder'
import qr from './qrcodeEncoder'

// Create an instance of our worker.
// The actual loading of the script gets handled
// by webpack's worker-loader:
// https://www.npmjs.com/package/worker-loader
const worker = new WorkerThread()

// The root element that contains our app markup
const rootElement = document.body.firstChild

let ms
let check = false
// any time we get a message from the worker
// it will be a set of "patches" to apply to
// the real DOM. We do this on a requestAnimationFrame
// for minimal impact
worker.onmessage = ({data: { payload, url }}) => {
  requestAnimationFrame(() => {
    applyPatch(rootElement, payload)
    // we only want to update the URL
    // if it's different than the current
    // URL. Otherwise we keep pushing
    // the same url to the history with
    // each render
    if (location.pathname !== url) {
      history.pushState(null, null, url)
      console.log(url)

      // SO MUCH HACKERY INVOLVED FOR CAMERA
      if (url !== '/scan.html' && ms) {
        ms.getTracks().forEach((track) => {
          track.stop()
        })
        ms = undefined
        qrDecoder.stop()
      }
      if (url === '/scan.html') {
        importQrCode()
      }

      if (url === '/export') {
        const image = document.getElementById('export-img')
        qr.image({
          image,
          value: image['data-src']
        })
      }
    } else {
      if (url === '/scan.html' && !check) {
        importQrCode()
        check = true
      }
    }
  })
}

// we start things off by sending a virtual DOM
// representation of the *real* DOM along with
// the current URL to our worker
worker.postMessage({
  type: 'start',
  payload: {
    virtualDom: toJson(virtualize(rootElement)),
    url: location.pathname
  }
})

// listen for all clicks globally
document.body.addEventListener('click', (event) => {
  const click = event.target['data-click']
  const pathname = getLocalPathname(event)
  if (click) {
    event.preventDefault()
    worker.postMessage(click)
  } else if (pathname) {
    event.preventDefault()
    worker.postMessage({type: 'setUrl', payload: pathname})
  }
})

document.body.addEventListener('submit', (event) => {
  const importForm = event.target.matches('#import-form')
  if (importForm) {
    event.preventDefault()
    worker.postMessage({
      type: 'import',
      payload: {
        name: event.target.name.value,
        seed: event.target.seed.value
      }
    })
  } else {
    const loginForm = event.target.matches('#login')
    if (loginForm) {
      event.preventDefault()
      event.stopPropagation()
      event.stopImmediatePropagation()
      worker.postMessage({
        type: 'login',
        payload: {
          password: event.target.password.value
        }
      })
    }
  }
})

function requestBackCam () {
  return navigator.mediaDevices.enumerateDevices()
    .then((devices) => {
      let id
      devices.forEach((device) => {
        if (device.kind === 'videoinput') {
          id = device.deviceId
          if (device.label.includes('back')) {
            return
          }
        }
      })
      return id
    })
    .then((id) => {
      return navigator.mediaDevices.getUserMedia({
        audio: false,
        video: {
          optional: [{
            sourceId: id
          }]
        }
      })
    })
}

function importQrCode() {
  return requestBackCam().then((mediaStream) => {
    ms = mediaStream
    const video = document.querySelector('video')
    video.src = window.URL.createObjectURL(mediaStream)
    video.onloadedmetadata = () => {
      video.play()

      qrDecoder.decode(video).then((result) => {
        if (result) {
          const a = new URL(result)
          const name = a.pathname.substr(7)
          const b = new URLSearchParams(a.search.substr(1))
          const seed = b.get('secret')
          worker.postMessage({
            type: 'import',
            payload: {
              name: decodeURI(name),
              seed: seed
            }
          })
        }
      }, true)
    }
  })
  .catch((err) => {
    console.error(err)
    worker.postMessage({type: 'setUrl', payload: '/import.html'})
  })
}
