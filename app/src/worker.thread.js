/*global self*/
import diff from 'virtual-dom/diff'

import serializePatch from 'vdom-serialized-patch/serialize'
import fromJson from 'vdom-as-json/fromJson'
import app from './views/app'

let currentVDom
let renderCount = 0

// our entire application model.state
// as a plain object
import * as model from './state'

const updateDom = () => {
  // just for fun
  console.log('render count:', ++renderCount)
  // our entire app in one line:
  const newVDom = app(model.state)

  // do the diff
  const patches = diff(currentVDom, newVDom)

  // cache last vdom so we diff against
  // the new one the next time through
  currentVDom = newVDom

   // send patches and current url back to the main thread
  self.postMessage({url: model.state.url, payload: serializePatch(patches)})
}

self.onmessage = ({data}) => {
  const { type, payload } = data
  console.log(model.state.hasKey, type)
  if (!model.state.hasKey && type !== 'login') {
    model.state.url = '/'
    updateDom()
  } else {
    switch (type) {
      case 'start': {
        currentVDom = fromJson(payload.virtualDom)
        model.state.url = payload.url
        updateDom()
        break
      }
      case 'setUrl': {
        model.state.url = payload
        updateDom()
        break
      }
      case 'import': {
        model.state.url = '/'
        model.add(payload).then(model.createTokens).then(updateDom)
        break
      }
      case 'updateTokens': {
        model.createTokens().then(updateDom)
        break
      }
      case 'delete': {
        model.remove(payload).then(updateDom)
        break
      }
      case 'export': {
        model.state.url = '/export'
        model.exportItem(payload).then(updateDom)
        break
      }
      case 'login': {
        model.state.url = '/'
        const password = payload.password
        model.initialisation(password)
        .then(updateDom)
        break
      }
      case 'deleteDatabase': {
        model.state.url = '/'
        model.deleteDatabase()
        .then(updateDom)
        break
      }
    }
  }
}
