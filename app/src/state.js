import localforage from 'localforage'
import authenticator from './authenticator'
import zxcvbn from 'zxcvbn'

const store = localforage.createInstance({
  name: 'v1.0.0'
})

export function deleteDatabase() {
  return store.clear()
}

let key

export const state = {
  nameSeedPairs: [],
  url: '/',
  hasKey: false,
  wrongPassword: undefined,
  weakPassword: undefined,
  passwordTips: undefined
}

function passwordInformation(password) {
  return zxcvbn(password)
}

export function isPasswordStrong(password) {
  /**
   * 0 # too guessable: risky password. (guesses < 10^3)
   * 1 # very guessable: protection from throttled online attacks. (guesses < 10^6)
   * 2 # somewhat guessable: protection from unthrottled online attacks. (guesses < 10^8)
   * 3 # safely unguessable: moderate protection from offline slow-hash scenario. (guesses < 10^10)
   * 4 # very unguessable: strong protection from offline slow-hash scenario. (guesses >= 10^10)
  */
	return zxcvbn(password).score > 3
}

function encrypt (data) {
  //Don't re-use initialization vectors!
  //Always generate a new iv every time your encrypt!
  var iv = crypto.getRandomValues(new Uint8Array(16));
  return crypto.subtle.encrypt({name: 'AES-CBC', iv: iv }, key, convertStringToArrayBufferView(data))
  .then(d => {
    return [d, iv]
  })
}

function decrypt (data, iv) {
  return crypto.subtle.decrypt({ name: "AES-CBC", iv: iv }, key, data)
  .then(data => {
    return arrayBufferToString(data)
  })
}

function removeKey () {
  key = undefined
  state.hasKey = false;
}

function saveKey (k) {
  key = k
  state.hasKey = true;
}

function createKey (password, saltBuffer) {
  // salt should be Uint8Array or ArrayBuffer
  // var saltBuffer = convertStringToArrayBufferView('randomly generated salt `crypto.getRandomValues(new Uint8Array(50))`, created during initialisation if not already present, stored in database and exportable for moving to a new device.');

  var passphraseKey = convertStringToArrayBufferView(password);

  return crypto.subtle.importKey('raw', passphraseKey, 'PBKDF2', false, ['deriveBits', 'deriveKey'])
  .then(function (key) {
    return crypto.subtle.deriveKey({ "name": 'PBKDF2', "salt": saltBuffer, "iterations": 10000, "hash": 'SHA-256' }, key,{ "name": 'AES-CBC', "length": 256 }, false, ["encrypt", "decrypt"])
  })
}

function convertStringToArrayBufferView(str) {
	return new TextEncoder("utf-8").encode(str);
}

function arrayBufferToString(arrayBuffer) {
	return new TextDecoder('utf-8').decode(arrayBuffer);
}

function load () {
  return store.keys().then((names) => {
    names = names.filter(name => name !== 'salt')
    const seeds = names.map(name => store.getItem(name)
    .then(([data, iv]) => {
      return decrypt(data, iv)
    }))
    return Promise.all(seeds).then((seeds) => {
      state.nameSeedPairs = seeds.map((seed, index) => ({
        name: denormaliseSeedName(names[index]),
        seed
      }))
    })
  })
}

function denormaliseSeedName (name) {
  return name.slice(0, -5)
}

function createSeedName (name) {
  return name + '-seed'
}

export function add ({name, seed}) {
  const n = createSeedName(name)
  return encrypt(seed)
  .then(data => {
    return store.setItem(n, data).then(() => {
      state.nameSeedPairs = state.nameSeedPairs.concat({name, seed})
    })
  })
}

export function remove ({name, seed}) {
  const n = createSeedName(name)
  return store.removeItem(n).then(() => {
    state.nameSeedPairs = state.nameSeedPairs.filter(({name: n, seed: s}) => {
      return n !== name && s !== seed
    })
  })
}

export function exportItem (nameSeedPair) {
  state.export = state.nameSeedPairs.find(({name, seed}) => name === nameSeedPair.name && seed === nameSeedPair.seed)
  return Promise.resolve()
}

export const createToken = (nameSeedPair) => Object.assign(
	nameSeedPair,
  {
    token: authenticator.generateToken(nameSeedPair.seed)
  }
)

export const createTokens = () => {
  state.nameSeedPairs.map(createToken)
}

function generateSalt () {
  return store.getItem('salt').then((salt) => {
    if (!salt) {
      return store.setItem('salt', crypto.getRandomValues(new Uint8Array(50)))
    } else {
      return salt
    }
  })
}

export function initialisation (password) {
  delete state.wrongPassword
  delete state.weakPassword
  return Promise.resolve()
  .then(() => {
    if (isPasswordStrong(password)) {
      return generateSalt()
      .then(salt => createKey(password, salt))
      .then(saveKey)
      .catch(console.error)
      .then(load)
      .then(createTokens)
      .catch(e => {
        console.error(e)
        removeKey()
        state.wrongPassword = true
      })
    } else {
      state.weakPassword = true
      state.passwordTips = passwordInformation(password).feedback.suggestions.join(' ')
    }
  })
}