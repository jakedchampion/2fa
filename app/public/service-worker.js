/* eslint-env serviceworker */
/* global fetch, Request, URL, location */
'use strict'

const version = 35
const CURRENT_CACHES = {
  'read-through': `read-through-cache-v${version}`,
  prefetch: `prefetch-cache-v${version}`
}

self.addEventListener('install', (event) => {
  var now = Date.now()

  var urlsToPrefetch = [
    'images/lock.png',
    'images/lock-192.png',
    'images/lock.svg',
    'manifest.json',
    'service-worker.js',
    '/',
    'index.html',
    'import.html',
    'scan.html',
    'about.html',
    '2fa.1.0.0.js',
    '2fa.1.0.0.css',
    '5346526372a99d315324.worker.js'
  ]

  console.log('Handling install event. Resources to prefetch:', urlsToPrefetch)

  event.waitUntil(
    caches.open(CURRENT_CACHES.prefetch).then((cache) => {
      var cachePromises = urlsToPrefetch.map((urlToPrefetch) => {
        // This constructs a new URL object using the service worker's script location as the base
        // for relative URLs.
        var url = new URL(urlToPrefetch, location.href)
        // Append a cache-bust=TIMESTAMP URL parameter to each URL's query string.
        // This is particularly important when precaching resources that are later used in the
        // fetch handler as responses directly, without consulting the network (i.e. cache-first).
        // If we were to get back a response from the HTTP browser cache for this precaching request
        // then that stale response would be used indefinitely, or at least until the next time
        // the service worker script changes triggering the install flow.
        url.search += (url.search ? '&' : '?') + 'cache-bust=' + now

        // It's very important to use {mode: 'no-cors'} if there is any chance that
        // the resources being fetched are served off of a server that doesn't support
        // CORS (http://en.wikipedia.org/wiki/Cross-origin_resource_sharing).
        // In this example, www.chromium.org doesn't support CORS, and the fetch()
        // would fail if the default mode of 'cors' was used for the fetch() request.
        // The drawback of hardcoding {mode: 'no-cors'} is that the response from all
        // cross-origin hosts will always be opaque
        // (https://slightlyoff.github.io/ServiceWorker/spec/service_worker/index.html#cross-origin-resources)
        // and it is not possible to determine whether an opaque response represents a success or failure
        // (https://github.com/whatwg/fetch/issues/14).
        var request = new Request(url, {mode: 'no-cors'})
        return fetch(request).then((response) => {
          if (response.status >= 400) {
            throw new Error('request for ' + urlToPrefetch +
              ' failed with status ' + response.statusText)
          }

          // Use the original URL without the cache-busting parameter as the key for cache.put().
          return cache.put(urlToPrefetch, response)
        }).catch((error) => {
          console.error('Not caching ' + urlToPrefetch + ' due to ' + error)
        })
      })

      return Promise.all(cachePromises).then(() => {
        console.log('Pre-fetching complete.')
      })
    }).catch((error) => {
      console.error('Pre-fetching failed:', error)
    })
  )
})

self.addEventListener('activate', (event) => {
  // Delete all caches that aren't named in CURRENT_CACHES.
  // While there is only one cache in this example, the same logic will handle the case where
  // there are multiple versioned caches.
  const expectedCacheNames = Object.keys(CURRENT_CACHES).map((key) => CURRENT_CACHES[key])

  event.waitUntil(
    caches.keys().then((cacheNames) => Promise.all(
        cacheNames.map((cacheName) => {
          if (expectedCacheNames.indexOf(cacheName) === -1) {
            // If this cache name isn't present in the array of "expected" cache names, then delete it.
            console.log('Deleting out of date cache:', cacheName)
            return caches.delete(cacheName)
          }
        })
      )
    )
  )
})

self.addEventListener('fetch', (event) => {
  console.log('Handling fetch event for', event.request.url)

  event.respondWith(caches.match(event.request).then((response) => {
    if (response) {
      // If there is an entry in the cache for event.request, then response will be defined
      // and we can just return it.
      console.log(' Found response in cache:', response)

      return response
    }

    // Otherwise, if there is no entry in the cache for event.request, response will be
    // undefined, and we need to fetch() the resource.
    console.log(' No response for %s found in cache. ' +
        'About to fetch from network...', event.request.url)

    // We call .clone() on the request since we might use it in the call to cache.put() later on.
    // Both fetch() and cache.put() "consume" the request, so we need to make a copy.
    // (see https://fetch.spec.whatwg.org/#dom-request-clone)
    return fetch(event.request.clone()).then((freshResponse) => {
      console.log('  Response for %s from network is: %O', event.request.url, freshResponse)

      // Optional: add in extra conditions here, e.g. response.type == 'basic' to only cache
      // responses from the same domain. See https://fetch.spec.whatwg.org/#concept-response-type
      // if (freshResponse.status < 400) {
      //   // This avoids caching responses that we know are errors (i.e. HTTP status code of 4xx or 5xx).
      //   // One limitation is that, for non-CORS requests, we get back a filtered opaque response
      //   // (https://fetch.spec.whatwg.org/#concept-filtered-response-opaque) which will always have a
      //   // .status of 0, regardless of whether the underlying HTTP call was successful. Since we're
      //   // blindly caching those opaque responses, we run the risk of caching a transient error response.
      //   //
      //   // We need to call .clone() on the response object to save a copy of it to the cache.
      //   // (https://fetch.spec.whatwg.org/#dom-request-clone)
      //   caches.open(CURRENT_CACHES['read-through']).then((cache) => {
      //     cache.put(event.request, freshResponse.clone())
      //   })
      // }

      // Return the original response object, which will be used to fulfill the resource request.
      return freshResponse
    })
  }).catch((error) => {
    // This catch() will handle exceptions that arise from the match() or fetch() operations.
    console.error('  Read-through caching failed:', error)

    throw error
  }))
})
