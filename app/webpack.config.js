require('babel-register')
var getConfig = require('hjs-webpack')
var toHtml = require('vdom-to-html')
var app = require('./src/views/app').default
var initialState = require('./src/state').state

module.exports = getConfig({
  in: 'src/main.js',
  out: 'public',
  clearBeforeBuild: '!(service-worker.js|manifest.json|images|Dockerfile)',
  html: function (context) {
    function render (state) {
      return context.defaultTemplate({
        lang: 'en-GB',
        html: toHtml(app(state)),
        title: '2FA Codes',
        metaTags: {
          'theme-color': "#3F51B5"
        },
        head: `<link rel="manifest" href="/manifest.json">
  <link rel="icon" type="image/png" href="images/lock.png">
  <link rel="canonical" href="/">
  <script>
  // Helper function which returns a promise which resolves once the service worker registration
  // is past the "installing" state.
  function waitUntilInstalled(registration) {
    return new Promise(function(resolve, reject) {
      if (registration.installing) {
        // If the current registration represents the "installing" service worker, then wait
        // until the installation step (during which the resources are pre-fetched) completes
        // to display an installed message.
        registration.installing.addEventListener('statechange', function(e) {
          if (e.target.state == 'installed') {
            resolve();
          } else if(e.target.state == 'redundant') {
            reject();
          }
        });
      } else {
        // Otherwise, if this isn't the "installing" service worker, then installation must have been
        // completed during a previous visit to this page, and the resources are already pre-fetched.
        // So we can show the list of files right away.
        resolve();
      }
    });
  }
  if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('./service-worker.js', {scope: './'})
    .then(waitUntilInstalled)
    .then(() => {
      console.log('installed service worker')
    })
  }
</script>`
      })
    }

    return {
      'about.html': render({url: '/about.html'}),
      'scan.html': render({url: '/scan.html'}),
      'import.html': render({url: '/import.html'}),
      'index.html': render(initialState)
    }
  }
})
